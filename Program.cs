using Finanzas.Server;
using Finanzas.Server.Data;
using MediatR;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpLogging;

static string getConnectionString()
{
    var user = Environment.GetEnvironmentVariable("DB_USER") ?? "finanzas_admin";
    var password = Environment.GetEnvironmentVariable("DB_PASSWORD") ?? "finanzas_admin";
    var host = Environment.GetEnvironmentVariable("DB_HOST") ?? "localhost";
    var port = Environment.GetEnvironmentVariable("DB_PORT") ?? "5432";
    var database = Environment.GetEnvironmentVariable("DB_NAME") ?? "finanzas";
    var cs = $"Server={host}; port={port}; user id = {user}; password = {password}; database={database}; pooling = true; sslmode=disable; Trust Server Certificate=true;";
    return cs;
}

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddHttpLogging(logging =>
{
    logging.LoggingFields = HttpLoggingFields.All;
    logging.RequestBodyLogLimit = 4096;
    logging.ResponseBodyLogLimit = 4096;
});

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

string connectionString = getConnectionString();
builder.Services.AddDbContext<ApplicationDbContext>(p => p.UseNpgsql(connectionString));
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidAudience = JWT.Audience,
        ValidIssuer = JWT.Issuer,
        IssuerSigningKey = JWT.Key
    };
});
builder.Services.AddAuthorization();

builder.Host.ConfigureAppConfiguration(conf =>
  {
      conf.SetBasePath(AppDomain.CurrentDomain.BaseDirectory);
  });

var app = builder.Build();

app.UseCors(builder => builder
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.MigrateDatabase();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpLogging();

#if (DEBUG)
app.Use(async (context, next) =>
{
    context.Request.EnableBuffering();
    var bodyText = await new StreamReader(context.Request.Body).ReadToEndAsync();
    Console.Write($"Request: {context.Request.Method} {context.Request.Path} ");
    Console.WriteLine($"Body: {bodyText}");
    context.Request.Body.Seek(0, SeekOrigin.Begin);
    await next();
});
#endif

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

/*app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
{
    var forecast =  Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast")
.WithOpenApi();
*/

app.Run();

record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}
