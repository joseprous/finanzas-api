using Finanzas.Models;
using Microsoft.EntityFrameworkCore;

namespace Finanzas.Server.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {

    }

    public DbSet<Movement> Movements => Set<Movement>();
    public DbSet<Category> Categories => Set<Category>();
    public DbSet<SubCategory> SubCategories => Set<SubCategory>();

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        var categories = new List<(string Name, List<string> SubCategories)>
        {
            ("Salario José", new List<string>()),
            ("Salario Ana", new List<string>()),
            ("Ingresos Extras", new List<string>()),

            ("Supermercado", new List<string>()),
            ("Alimentación", new List<string>{
                    "Agua",
                    "Para llevar",
                    "Delivery",
                    "Salidas: Diurnas",
                    "Salidas: Nocturnas"
                    }),
            ("Gastos extras", new List<string>()),
            ("Educación", new List<string>{
                   "Idiomas",
                   "Formación Profesional"
                   }),
            ("Clubes", new List<string>{
                   "Cuotas Mensuales",
                   "Gastos Extraordinarios"
                   }),
           ("Cuidado personal", new List<string>{
                   "Peluquería",
                   "Sauna/Masajes",
                   "Deportes",
                   "Insumos",
                   "Perfumes"
                   }),
           ("Mantenimiento Casa", new List<string>{
                   "Telefonía e internet",
                   "Servicios básicos",
                   "Limpieza y cocina",
                   "Jardinería",
                   "Construcción",
                   "Reparación"
                   }),
           ("Mantenimiento Vehículos", new List<string>{
                   "Service",
                   "Repuestos",
                   "Mecánico",
                   "Lavado",
                   "Combustible",
                   "Seguro"
                   }),
           ("Mascotas", new List<string>{
                   "Baño",
                   "Salud",
                   "Accesorios"
                   }),
           ("Salud", new List<string>{
                  "Farmacia",
                  "Seguro",
                  "Sin Cobertura: Estudios",
                  "Sin Cobertura: Consultas",
                  "Sin Cobertura: Tratamientos"
                  }),
           ("Vestimenta", new List<string>()),
           ("Entretenimiento", new List<string>()),
           ("Caídas Trabajo", new List<string>()),
           ("Ayuda Familiar", new List<string>()),
           ("Terrenos", new List<string>()),
           ("Viajes", new List<string>{
                  "Nacional",
                  "Internacional",
                  "Paseo"
                  }),
           ("Gastos Administrativos", new List<string>()),
        };
        var subCategoryId = 1;
        for (int i = 0; i < 3; i++)
        {
            var category = new Category
            {
                Id = i + 1,
                Name = categories[i].Name,
                Type = MovementType.Income
            };
            modelBuilder.Entity<Category>().HasData(category);
        }
        for (int i = 3; i < categories.Count; i++)
        {
            var category = new Category
            {
                Id = i + 1,
                Name = categories[i].Name,
                Type = MovementType.Expense
            };
            modelBuilder.Entity<Category>().HasData(category);
            foreach (var name in categories[i].SubCategories)
            {
                var subCategory = new SubCategory
                {
                    Id = subCategoryId++,
                    CategoryId = category.Id,
                    Name = name,
                };
                modelBuilder.Entity<SubCategory>().HasData(subCategory);
            }
        }
        modelBuilder.Entity<SubCategory>().HasData(new SubCategory
        {
            Id = subCategoryId++,
            CategoryId = 10, // mantenimiento casa
            Name = "Equipamiento",
        });

    }
}
