namespace Finanzas.Models;

public enum MovementType
{
    Income,
    Expense,
    Adjustment
}

public class Movement
{
    public int Id { get; set; }
    public DateTime Date { get; set; }
    public Decimal Amount { get; set; }
    public MovementType Type { get; set; }
    public Category Category { get; set; }
    public SubCategory? SubCategory { get; set; }
    public string Notes { get; set; } = string.Empty;
}
