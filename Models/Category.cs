namespace Finanzas.Models;

public class Category
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public List<SubCategory> SubCategories { get; set; } = new List<SubCategory>();
    public MovementType Type { get; set; }
}
