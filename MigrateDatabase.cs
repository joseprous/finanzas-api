using Microsoft.EntityFrameworkCore;
using Finanzas.Server.Data;

namespace Finanzas.Server;

public static class MigrateDatabaseExtension
{
    public static IHost MigrateDatabase(this IHost webHost)
    {
        var serviceScopeFactory = (IServiceScopeFactory?)webHost.Services.GetService(typeof(IServiceScopeFactory));

        if (serviceScopeFactory == null) throw new Exception("serviceScopeFactory is null");

        using (var scope = serviceScopeFactory.CreateScope())
        {
            var services = scope.ServiceProvider;
            var dbContext = services.GetRequiredService<ApplicationDbContext>();

            dbContext.Database.Migrate();

        }

        return webHost;
    }
}
