using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Finanzas.Server.Movements.Queries;
using Finanzas.Server.Movements.Commands;

namespace Finanzas.Server.Controllers;

// [Authorize]
public class MovementsController : ApiControllerBase
{
    private readonly ILogger<MovementsController> _logger;

    public MovementsController(ILogger<MovementsController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public async Task<ActionResult<GetMovementsResponse>> GetMovements([FromQuery] GetMovementsQuery query)
    {
        return await Mediator.Send(query);
    }

    [HttpGet("plain")]
    public async Task<ActionResult<GetMovementsPlainResponse>> GetMovementsPlain([FromQuery] GetMovementsPlainQuery query)
    {
        return await Mediator.Send(query);
    }

    [HttpPost]
    public async Task<ActionResult<int>> Create(CreateMovementCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpPut]
    public async Task<ActionResult<int>> Update(UpdateMovementCommand command)
    {
        return await Mediator.Send(command);
    }

    [HttpDelete]
    public async Task<ActionResult> Delete(DeleteMovementCommand command)
    {
        await Mediator.Send(command);
        return NoContent();
    }
}
