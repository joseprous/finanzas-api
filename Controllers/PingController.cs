using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Finanzas.Server.Controllers;

public class PingController : ApiControllerBase
{
    private readonly ILogger<PingController> _logger;

    public PingController(ILogger<PingController> logger)
    {
        _logger = logger;
    }

    public async Task<ActionResult<string>> GetPing()
    {
        return "ok";
    }

}
