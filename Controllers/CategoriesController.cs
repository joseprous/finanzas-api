using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Finanzas.Server.Categories.Queries;
using Finanzas.Models;

namespace Finanzas.Server.Controllers;

// [Authorize]
public class CategoriesController : ApiControllerBase
{
    private readonly ILogger<MovementsController> _logger;

    public CategoriesController(ILogger<MovementsController> logger)
    {
        _logger = logger;
    }

    [HttpGet]
    public async Task<ActionResult<List<CategoryDto>>> GetMovements([FromQuery] GetCategoriesQuery query)
    {
        return await Mediator.Send(query);
    }

}
