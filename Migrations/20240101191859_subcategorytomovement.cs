﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FinanzasAPI.Migrations
{
    /// <inheritdoc />
    public partial class subcategorytomovement : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubCategoryId",
                table: "Movements",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Movements_SubCategoryId",
                table: "Movements",
                column: "SubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Movements_SubCategories_SubCategoryId",
                table: "Movements",
                column: "SubCategoryId",
                principalTable: "SubCategories",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movements_SubCategories_SubCategoryId",
                table: "Movements");

            migrationBuilder.DropIndex(
                name: "IX_Movements_SubCategoryId",
                table: "Movements");

            migrationBuilder.DropColumn(
                name: "SubCategoryId",
                table: "Movements");
        }
    }
}
