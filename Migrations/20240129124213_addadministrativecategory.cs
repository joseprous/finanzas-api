﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FinanzasAPI.Migrations
{
    /// <inheritdoc />
    public partial class addadministrativecategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[] { 20, "Gastos Administrativos", 1 });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: 20);
        }
    }
}
