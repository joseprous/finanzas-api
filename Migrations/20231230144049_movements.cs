﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace FinanzasAPI.Migrations
{
    /// <inheritdoc />
    public partial class movements : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movements",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Amount = table.Column<decimal>(type: "numeric", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<int>(type: "integer", nullable: false),
                    Notes = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movements_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Supermercado" },
                    { 2, "Farmacia" },
                    { 3, "Agua" },
                    { 4, "Comidas: para llevar" },
                    { 5, "Comidas: delivery" },
                    { 6, "Salidas: diurna" },
                    { 7, "Salidas: nocturnas" },
                    { 8, "Gastos extras" },
                    { 9, "Combustible" },
                    { 10, "Gastos fijos: compras a cuotas" },
                    { 11, "Gastos fijos: clubes" },
                    { 12, "Gastos fijos: Telefonía e internet" },
                    { 13, "Gastos fijos: servicios básicos " },
                    { 14, "Gastos fijos: limpieza de la casa" },
                    { 15, "Gastos fijos: jardinería" },
                    { 16, "Gastos fijos: deportes" },
                    { 17, "Gastos fijos: cuidado personal" },
                    { 18, "Gastos fijos: educación" },
                    { 19, "Ingreso: salario José" },
                    { 20, "Ingreso: salario Ana" },
                    { 21, "Ingreso: extras" },
                    { 22, "Mantenimiento Casa" },
                    { 23, "Mantenimiento Vehiculos" },
                    { 24, "Mascotas" },
                    { 25, "Salud" },
                    { 26, "Vestimenta" },
                    { 27, "Entretenimiento" },
                    { 28, "Caidas Trabajo" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movements_CategoryId",
                table: "Movements",
                column: "CategoryId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movements");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
