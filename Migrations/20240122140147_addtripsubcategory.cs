﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FinanzasAPI.Migrations
{
    /// <inheritdoc />
    public partial class addtripsubcategory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SubCategories",
                columns: new[] { "Id", "CategoryId", "Name" },
                values: new object[] { 37, 19, "Paseo" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SubCategories",
                keyColumn: "Id",
                keyValue: 37);
        }
    }
}
