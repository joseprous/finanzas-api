using MediatR;
using Finanzas.Models;
using Microsoft.EntityFrameworkCore;
using Finanzas.Server.Data;

namespace Finanzas.Server.Categories.Queries;

public class GetCategoriesQuery : IRequest<List<CategoryDto>>
{
    public MovementType? Type { get; set; }
}

public class SubCategoryDto
{
    public int Id { get; set; }
    public int CategoryId { get; set; }
    public string Name { get; set; } = string.Empty;
}

public class CategoryDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public List<SubCategoryDto> SubCategories { get; set; } = new List<SubCategoryDto>();
    public MovementType Type { get; set; }
}

public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, List<CategoryDto>>
{
    private readonly ApplicationDbContext _context;

    public GetCategoriesQueryHandler(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<List<CategoryDto>> Handle(GetCategoriesQuery request, CancellationToken cancellationToken)
    {
        var categories = _context.Categories.Include(x => x.SubCategories);
        return await categories
        .Where(c => request.Type == null || c.Type == request.Type)
        .OrderBy(c => c.Name)
        .Select(c => new CategoryDto
        {
            Id = c.Id,
            Name = c.Name,
            SubCategories = c.SubCategories
             .OrderBy(sc => sc.Name)
             .Select(sc => new SubCategoryDto
             {
                 Id = sc.Id,
                 CategoryId = sc.CategoryId,
                 Name = sc.Name
             }).ToList(),
            Type = c.Type
        }).ToListAsync();
    }
}
