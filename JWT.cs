using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Finanzas.Server;

public static class JWT
{
    public static string Issuer { get; } = "finanzas";
    public static string Audience { get; } = "client";
    public static SecurityKey Key { get; }
    = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("D5q74w6vSrVE56cuALJM")); // TODO use env variable


    public static string CreateJWT(string userId, string userName)
    {
        string subject = "accesstoken";
        var claims = new[] {
            new Claim(JwtRegisteredClaimNames.Sub, subject),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
            new Claim("UserId", userId),
            new Claim("UserName", userName)
        };

        var signIn = new SigningCredentials(Key, SecurityAlgorithms.HmacSha256);
        var token = new JwtSecurityToken(
            Issuer,
            Audience,
            claims,
            expires: DateTime.UtcNow.AddHours(24),
            signingCredentials: signIn);

        return new JwtSecurityTokenHandler().WriteToken(token); ;
    }

}
