using MediatR;
using Microsoft.EntityFrameworkCore;
using Finanzas.Models;
using Finanzas.Server;
using Finanzas.Server.Data;
using System.Globalization;

namespace Finanzas.Server.Movements.Commands;

public class DeleteMovementCommand : IRequest<Unit>
{
    public int Id { get; set; }
}

public class DeleteMovementCommandHandler : IRequestHandler<DeleteMovementCommand, Unit>
{
    private readonly ApplicationDbContext _context;

    public DeleteMovementCommandHandler(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<Unit> Handle(DeleteMovementCommand request, CancellationToken cancellationToken)
    {
        var entity = await _context.Movements
        .FindAsync(new object[] { request.Id }, cancellationToken);

        if (entity == null) throw new Exception($"Movement {request.Id} not found");

        _context.Movements.Remove(entity);

        await _context.SaveChangesAsync(cancellationToken);

        return Unit.Value;
    }
}
