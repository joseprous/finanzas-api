using MediatR;
using Microsoft.EntityFrameworkCore;
using Finanzas.Models;
using Finanzas.Server;
using Finanzas.Server.Data;
using System.Globalization;

namespace Finanzas.Server.Movements.Commands;

public class CreateMovementCommand : IRequest<int>
{
    public string Date { get; set; }
    public Decimal Amount { get; set; }
    public string Type { get; set; }
    public int CategoryId { get; set; }
    public int? SubCategoryId { get; set; }
    public string Notes { get; set; } = string.Empty;
}

public class CreateMovementCommandHandler : IRequestHandler<CreateMovementCommand, int>
{
    private readonly ApplicationDbContext _context;

    public CreateMovementCommandHandler(ApplicationDbContext context)
    {
        _context = context;
    }

    public async Task<int> Handle(CreateMovementCommand request, CancellationToken cancellationToken)
    {
        var category = await _context.Categories
            .Include(x => x.SubCategories)
            .FirstOrDefaultAsync(x => x.Id == request.CategoryId);

        if (category == null) throw new Exception($"Category {request.CategoryId} not found");

        SubCategory? subcategory = null;
        if (request.SubCategoryId != null)
        {
            subcategory = category.SubCategories
                    .FirstOrDefault(x => x.Id == request.SubCategoryId);
            if (subcategory == null) throw new Exception($"SubCategory {request.SubCategoryId} not found");
        }
        MovementType type;
        DateTime date;
        if (!DateTime.TryParseExact(request.Date,
                   "yyyy/MM/dd",
                   CultureInfo.InvariantCulture,
                   DateTimeStyles.AdjustToUniversal,
                   out date))
        {
            throw new Exception($"Error parsing Date {request.Date}");
        }

        var entity = new Movement
        {
            Date = DateTime.SpecifyKind(date, DateTimeKind.Utc),
            Amount = request.Amount,
            Category = category,
            SubCategory = subcategory,
            Notes = request.Notes
        };

        if (Enum.TryParse(request.Type, true, out type))
        {
            entity.Type = type;
        }
        else
        {
            throw new Exception($"Error parsing MovementType {request.Type}");
        }

        _context.Movements.Add(entity);
        await _context.SaveChangesAsync(cancellationToken);

        return entity.Id;
    }
}
