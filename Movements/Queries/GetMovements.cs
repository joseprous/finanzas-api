using MediatR;
using Finanzas.Models;
using Microsoft.EntityFrameworkCore;
using Finanzas.Server.Data;

namespace Finanzas.Server.Movements.Queries;

public class GetMovementsQuery : IRequest<GetMovementsResponse>
{
    public string StartMonth { get; set; } = string.Empty;
    public string EndMonth { get; set; } = string.Empty;
}

public class CategoryDto
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Decimal Total { get; set; }
    public List<SubCategoryDto> SubCategories { get; set; } = new List<SubCategoryDto>();
}

public class SubCategoryDto
{
    public int? Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public Decimal Total { get; set; }
    public List<MovementDto> Movements { get; set; } = new List<MovementDto>();
}

public class MovementDto
{
    public int Id { get; set; }
    public string Date { get; set; } = string.Empty;
    public string Category { get; set; } = string.Empty;
    public Decimal Amount { get; set; }
    public string Notes { get; set; } = string.Empty;
}

public class GetMovementsResponse
{
    public Decimal TotalIncome { get; set; }
    public Decimal TotalExpense { get; set; }
    public Decimal TotalAdjustment { get; set; }
    public Decimal TotalDifference { get; set; }
    public Decimal AccumulatedDifference { get; set; }
    public List<CategoryDto> ExpenseCategories { get; set; } = new List<CategoryDto>();
    public List<MovementDto> Incomes { get; set; } = new List<MovementDto>();
    public List<MovementDto> Adjustments { get; set; } = new List<MovementDto>();
}

public class GetMovementsQueryHandler : IRequestHandler<GetMovementsQuery, GetMovementsResponse>
{
    private readonly ApplicationDbContext _context;

    public GetMovementsQueryHandler(ApplicationDbContext context)
    {
        _context = context;
    }

    public struct Totals
    {
        public Decimal Income;
        public Decimal Expense;
        public Decimal Difference;
        public Decimal Adjustment;
    };

    private Totals GetTotals(IQueryable<Movement> movements)
    {
        var totalIncome = movements.Where(x => x.Type == MovementType.Income).Sum(m => m.Amount);
        var totalExpense = movements.Where(x => x.Type == MovementType.Expense).Sum(m => m.Amount);
        var totalAdjustment = movements.Where(x => x.Type == MovementType.Adjustment).Sum(m => m.Amount);
        var totalDifference = totalIncome - totalExpense + totalAdjustment;
        return new Totals
        {
            Income = totalIncome,
            Expense = totalExpense,
            Difference = totalDifference,
            Adjustment = totalAdjustment
        };
    }

    private DateTime ParseMonth(string monthString)
    {
        int year;
        int month;
        if (string.IsNullOrEmpty(monthString))
        {
            year = DateTime.Now.Year;
            month = DateTime.Now.Month;
        }
        else
        {
            var subs = monthString.Split('/');
            year = int.Parse(subs[0]);
            month = int.Parse(subs[1]);
        }
        return DateTime.SpecifyKind(new DateTime(year, month, 1), DateTimeKind.Utc);
    }

    public async Task<GetMovementsResponse> Handle(GetMovementsQuery request, CancellationToken cancellationToken)
    {
        var start = ParseMonth(request.StartMonth);
        var end = ParseMonth(request.EndMonth).AddMonths(1).AddDays(-1);

        var movements = _context.Movements
                .Include(x => x.Category)
                .Include(x => x.SubCategory)
                .Where(m => m.Date >= start && m.Date <= end);

        var previousMovements = _context.Movements.Where(m => m.Date < start);
        var previousTotals = GetTotals(previousMovements);

        var incomeMovements = await movements
            .Where(x => x.Type == MovementType.Income)
            .OrderByDescending(x => x.Date.Date)
            .ThenByDescending(x => x.Id)
            .ToListAsync();
        var expenseMovements = await movements
            .Where(x => x.Type == MovementType.Expense)
            .OrderByDescending(x => x.Date.Date)
            .ThenByDescending(x => x.Id)
            .ToListAsync();
        var adjustmentMovements = await movements
            .Where(x => x.Type == MovementType.Adjustment)
            .OrderByDescending(x => x.Date.Date)
            .ThenByDescending(x => x.Id)
            .ToListAsync();

        var totals = GetTotals(movements);
        return new GetMovementsResponse
        {
            TotalIncome = totals.Income,
            TotalExpense = totals.Expense,
            TotalDifference = totals.Difference,
            TotalAdjustment = totals.Adjustment,
            AccumulatedDifference = totals.Difference + previousTotals.Difference,
            ExpenseCategories = expenseMovements.GroupBy(
                m => m.Category,
                (category, movements) => new CategoryDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    Total = movements.Sum(m => m.Amount),
                    SubCategories = movements.GroupBy(
                        m => m.SubCategory,
                        (subCategory, movements) => new SubCategoryDto
                        {
                            Id = subCategory?.Id,
                            Name = subCategory?.Name ?? "",
                            Total = movements.Sum(m => m.Amount),
                            Movements = movements
                            .OrderByDescending(x => x.Date.Date)
                            .Select(m => new MovementDto
                            {
                                Id = m.Id,
                                Date = m.Date.ToString("dd/MM/yyyy"),
                                Amount = m.Amount,
                                Notes = m.Notes
                            }).ToList()
                        }).OrderBy(x => x.Name).ToList()
                }).OrderBy(x => x.Name).ToList(),
            Incomes = incomeMovements.Select(m => new MovementDto
            {
                Id = m.Id,
                Date = m.Date.ToString("dd/MM/yyyy"),
                Category = m.Category?.Name ?? "",
                Amount = m.Amount,
                Notes = m.Notes
            }).ToList(),
            Adjustments = adjustmentMovements.Select(m => new MovementDto
            {
                Id = m.Id,
                Date = m.Date.ToString("dd/MM/yyyy"),
                Amount = m.Amount,
                Notes = m.Notes
            }).ToList()
        };
    }
}
