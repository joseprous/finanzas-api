using MediatR;
using Finanzas.Models;
using Microsoft.EntityFrameworkCore;
using Finanzas.Server.Data;

namespace Finanzas.Server.Movements.Queries;

public class GetMovementsPlainQuery : IRequest<GetMovementsPlainResponse>
{
    public string StartMonth { get; set; } = string.Empty;
    public string EndMonth { get; set; } = string.Empty;
}

public class MovementPlainDto
{
    public int Id { get; set; }
    public string Type { get; set; } = string.Empty;
    public string Date { get; set; } = string.Empty;
    public string Category { get; set; } = string.Empty;
    public string SubCategory { get; set; } = string.Empty;
    public Decimal Amount { get; set; }
    public string Notes { get; set; } = string.Empty;
}

public class GetMovementsPlainResponse
{
    public List<MovementPlainDto> Movements { get; set; } = new List<MovementPlainDto>();
}

public class GetMovementsPlainQueryHandler : IRequestHandler<GetMovementsPlainQuery, GetMovementsPlainResponse>
{
    private readonly ApplicationDbContext _context;

    public GetMovementsPlainQueryHandler(ApplicationDbContext context)
    {
        _context = context;
    }

    private DateTime ParseMonth(string monthString)
    {
        int year;
        int month;
        if (string.IsNullOrEmpty(monthString))
        {
            year = DateTime.Now.Year;
            month = DateTime.Now.Month;
        }
        else
        {
            var subs = monthString.Split('/');
            year = int.Parse(subs[0]);
            month = int.Parse(subs[1]);
        }
        return DateTime.SpecifyKind(new DateTime(year, month, 1), DateTimeKind.Utc);
    }

    public async Task<GetMovementsPlainResponse> Handle(GetMovementsPlainQuery request, CancellationToken cancellationToken)
    {
        var start = ParseMonth(request.StartMonth);
        var end = ParseMonth(request.EndMonth).AddMonths(1).AddDays(-1);

        var movements = await _context.Movements
                .Include(x => x.Category)
                .Include(x => x.SubCategory)
                .Where(m => m.Date >= start && m.Date <= end)
            .OrderByDescending(x => x.Date.Date)
            .ToListAsync();


        return new GetMovementsPlainResponse
        {
            Movements = movements.Select(m => new MovementPlainDto
            {
                Id = m.Id,
                Type = m.Type.ToString(),
                Date = m.Date.ToString("dd/MM/yyyy"),
                Category = m.Category.Name,
                SubCategory = m.SubCategory?.Name ?? "",
                Amount = m.Amount,
                Notes = m.Notes
            }).ToList()
        };
    }
}
